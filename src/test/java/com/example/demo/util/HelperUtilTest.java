package com.example.demo.util;

import com.example.demo.DTO.PalindromeDTO;
import com.example.demo.model.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import com.example.demo.util.HelperUtil;

import java.util.*;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class HelperUtilTest {

    private static final HelperUtil helperUtil = new HelperUtil();

    @Test
    void palindromeList() {
        User sofia = new User("Sofia");
        User marghareta = new User("Marghareta");
        User mackan = new User("Mackan");
        User divid = new User("Divid");
        User david = new User("David");
        List<User> users = Arrays.asList(sofia, marghareta, mackan, divid, david);
        List<PalindromeDTO> palindromeDTOS = Arrays.asList(
                new PalindromeDTO(sofia.getFirstname(), false),
                new PalindromeDTO(marghareta.getFirstname(), false),
                new PalindromeDTO(mackan.getFirstname(), false),
                new PalindromeDTO(divid.getFirstname(), true),
                new PalindromeDTO(david.getFirstname(), false));

        List<PalindromeDTO> actualPalindromes = helperUtil.palindromeList(users);
        assertEquals(palindromeDTOS.size(), actualPalindromes.size());

        for (int i = 0; i < actualPalindromes.size(); i++) {
            assertEquals(palindromeDTOS.get(i).getName(), actualPalindromes.get(i).getName());
            assertEquals(palindromeDTOS.get(i).getIsPalindrome(), actualPalindromes.get(i).getIsPalindrome());
        }
    }

    @Test
    void reverseString() {
        assertEquals("olleh", helperUtil.reverseString("hello"));
        assertEquals("aeslehC", helperUtil.reverseString("Chelsea"));
        assertEquals("olasalo", helperUtil.reverseString("olasalo"));
        assertNotEquals("OlaSalo", helperUtil.reverseString("OlaSalo"));
        assertNotEquals("Good", helperUtil.reverseString("United"));
    }

    @Test
    void nthLargestNumber() {
        List<Integer> numbers = Arrays.asList(1, 50, 2, 30);
        assertEquals(50, helperUtil.nthLargestNumber(numbers, 0));
        assertEquals(30, helperUtil.nthLargestNumber(numbers, 1));
        assertNotEquals(30, helperUtil.nthLargestNumber(numbers, 3));
        assertNotEquals(1, helperUtil.nthLargestNumber(numbers, 2));

    }

    @Test
    void zerosNumber() {
        assertEquals("00100", helperUtil.zerosNumber(100));
        assertEquals("100000", helperUtil.zerosNumber(100000));
        assertNotEquals("10000", helperUtil.zerosNumber(100));
    }

    @Test
    void isPalindrome() {
        assertTrue(helperUtil.isPalindrome("divid"));
        assertTrue(helperUtil.isPalindrome("www"));
        assertTrue(helperUtil.isPalindrome("DiVid"));
        assertFalse(helperUtil.isPalindrome("Mackan"));
        assertFalse(helperUtil.isPalindrome("David"));
    }


}