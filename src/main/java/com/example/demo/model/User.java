package com.example.demo.model;

public class User {
    private int id;
    private String firstname;
    private String lastname;
    private String username;
    private int age;

    public User (){}

    public User(String firstname){
        this.firstname = firstname;
    }
    public User(int id, String firstname, String lastname, String username, int age) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.username = username;
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getUsername() {
        return username;
    }

    public int getAge() {
        return age;
    }
}
