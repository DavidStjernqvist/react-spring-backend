package com.example.demo.DTO;

public class PalindromeDTO {
    private String name;
    private boolean isPalindrome;

    public PalindromeDTO(){ }

    public PalindromeDTO(String name, boolean isPalindrome){
        this.name = name;
        this.isPalindrome = isPalindrome;
    }

    public String getName() {
        return name;
    }

    public boolean getIsPalindrome() {
        return isPalindrome;
    }
}
