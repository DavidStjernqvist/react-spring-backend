package com.example.demo.api;

import com.example.demo.model.User;
import com.example.demo.util.HelperUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/")
public class MethodsApi {

    @Autowired
    private HelperUtil helperUtil;

    @GetMapping("userswithreversednames")
    public ResponseEntity usersWithReversedNames() throws JsonProcessingException {
        List<User> users = helperUtil.getJsonConvertToUsers();
        return ResponseEntity.ok(
                //response
                helperUtil.palindromeList(users)
        );
    }

    @GetMapping("reversestring")
    public ResponseEntity reverseString(@RequestParam("string") String string) {
        return ResponseEntity.ok(
                //response
                helperUtil.reverseString(string));
    }

    @GetMapping("ispalindrome")
    public ResponseEntity isPalindrome(@RequestParam("string") String string) {
        return ResponseEntity.ok(
                //response
                helperUtil.isPalindrome(string)
        );
    }
    @GetMapping("padnumberwithzeroes")
    public ResponseEntity padNumberWithZeroes(@RequestParam("number") int number) {
        return ResponseEntity.ok(
                //response
                helperUtil.zerosNumber(number)
        );
    }

    @GetMapping("findnthlargestnumber")
    public ResponseEntity findNthLargestNumber(@RequestParam("numbers") List<Integer> numbers,
                                         @RequestParam("nthlargestnumber") int nthLargestNumber) {
        return ResponseEntity.ok(
                //response
                helperUtil.nthLargestNumber(numbers, nthLargestNumber - 1)
        );
    }

}
