package com.example.demo.util;

import com.example.demo.model.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.stereotype.Component;

import com.example.demo.DTO.PalindromeDTO;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class HelperUtil {

    public List<PalindromeDTO> palindromeList(List<User> users) {
        return users.stream()
                .map(user -> new PalindromeDTO(user.getFirstname(), isPalindrome(user.getFirstname())))
                .collect(Collectors.toList());
    }

    public String reverseString(String string){
        return new StringBuilder(string).reverse().toString();
    }


    public String zerosNumber(int number){
        return String.format("%05d", number);
    }

    public int nthLargestNumber(List<Integer> numbers, int nthLargestNumber){
        return sortNumbers(numbers).get(nthLargestNumber);
    }

    private List<Integer> sortNumbers(List<Integer> numbers){
        return numbers.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
    }

    public boolean isPalindrome(String string){
        String stringLower = string.toLowerCase();
        return stringLower.equals(reverseString(stringLower));
    }

    //Modified for accessing resources in jar file
    private String getUsers() {
        ClassLoader classLoader = this.getClass().getClassLoader();
            try{
//                File file = new File(Objects.requireNonNull(classLoader.getResource("users.json")).getFile());
                InputStream i = classLoader.getResourceAsStream("users.json");

                StringBuilder stringBuilder = new StringBuilder();
//                BufferedReader br = new BufferedReader(new FileReader(file));
                BufferedReader br = new BufferedReader(new InputStreamReader(i));
                String line;
                while ((line = br.readLine()) != null) {
                    stringBuilder.append(line);
                }
                return stringBuilder.toString();
            }catch (Exception e){
                e.printStackTrace();
            }
            return "";
    }

    public List<User> getJsonConvertToUsers() throws JsonProcessingException {
        return new ObjectMapper().readValue(getUsers(), new TypeReference<List<User>>() {});
    }
}
