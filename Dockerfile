FROM maven:3.6.3-openjdk-15 AS build
COPY src /usr/src/app/src
COPY pom.xml /usr/src/app
RUN mvn -q -f /usr/src/app/pom.xml clean package

FROM openjdk:15-jdk-oraclelinux8
COPY --from=build /usr/src/app/target/*.jar /usr/app/kundnojd.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/usr/app/kundnojd.jar"]